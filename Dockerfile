FROM docker.io/library/amazoncorretto:17.0.8-alpine3.18

COPY target/*.war /

CMD ["java", "-jar", "mangaService-0.0.1-SNAPSHOT.war"]