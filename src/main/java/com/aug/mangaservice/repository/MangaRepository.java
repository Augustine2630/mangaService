package com.aug.mangaservice.repository;

import com.aug.mangaservice.model.Manga;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MangaRepository extends JpaRepository<Manga, Long> {
}
