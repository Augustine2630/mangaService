package com.aug.mangaservice.controller;

import com.aug.mangaservice.DTO.PageFilter;
import com.aug.mangaservice.model.Manga;
import com.aug.mangaservice.service.MangaMessagingService;
import com.aug.mangaservice.service.MangaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/manga")
public class MangaController {

    private final Logger log = LoggerFactory.getLogger(MangaController.class);

    private final MangaService mangaService;
    private final MangaMessagingService mangaMessagingService;

    public MangaController(MangaService mangaService, MangaMessagingService mangaMessagingService) {
        this.mangaService = mangaService;
        this.mangaMessagingService = mangaMessagingService;
    }

    @GetMapping("/")
    public List<Manga> showAllManga(){
        return mangaService.getAllManga();
    }

    @PostMapping("/page")
    public void getMangaPage(@RequestBody PageFilter pageFilter) {
        mangaMessagingService.sendMessage(pageFilter.getTitle());
    }

}
