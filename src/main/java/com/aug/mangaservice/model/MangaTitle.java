package com.aug.mangaservice.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Data
public abstract class MangaTitle {

    private String name;

    private String description;

    private LocalDateTime releaseDate;

    private Boolean isOnGoing;

}
