package com.aug.mangaservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Map;

@Configuration
public class DatasourceConfig {

    private final String host;
    private final String username;
    private final String password;
    protected Map<String, String> databaseCreds;

    public DatasourceConfig(@Value("${spring.datasource.url}") String host)  {
        this.username = System.getenv("db_user");
        this.password = System.getenv("db_pass");
        this.host = host;
    }

//    @Bean
//    public DataSource getDataSource() {
//        return DataSourceBuilder.create()
//                .driverClassName("org.postgresql.Driver")
//                .url(host)
//                .username(username)
//                .password(password)
//                .build();
//    }
}
