package com.aug.mangaservice.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class RabbitConfiguration {

    private final String username;
    private final String password;

    private final String host;
    private final int port;

    static final String topicExchangeName = "test-exchange";

    static final String queueName = "manga-queue";


    public RabbitConfiguration(@Value("${spring.rabbitmq.host}") String host, @Value("${spring.rabbitmq.port}") int port) {
        this.host = host;
        this.port = port;
        this.username = System.getenv("rabbit_user");
        this.password = System.getenv("rabbit_pass");
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host);
        connectionFactory.setPort(port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        return connectionFactory;
    }

    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("first.key");
    }


}
