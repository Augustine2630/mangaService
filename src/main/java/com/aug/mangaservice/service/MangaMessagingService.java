package com.aug.mangaservice.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MangaMessagingService {

    private final MessageProducer messageProducer;

    private static String queueName;

    public MangaMessagingService(MessageProducer messageProducer, @Value("${manga.messaging.queue}") String queueName) {
        this.messageProducer = messageProducer;
        MangaMessagingService.queueName = queueName;
    }


    public void sendMessage(String message) {
        messageProducer.sendMessageToQueue(queueName, message);
    }



}
