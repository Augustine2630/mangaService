package com.aug.mangaservice.service;

import com.aug.mangaservice.model.Manga;
import com.aug.mangaservice.repository.MangaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public record MangaService(MangaRepository mangaRepository) {

    public List<Manga> getAllManga(){
        return mangaRepository.findAll();
    }

}
